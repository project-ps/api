#!/usr/bin/env bash

startContainer() {
    docker run --publish=80:3000 --name=api -d paresport/api:1.0
}

buildContainer() {
    docker build -t paresport/api:1.0 .
}

stopContainer() {
    docker stop api
}

pushService() {
    aws deploy push --application-name ParesportApi --s3-location s3://paresport-api/Paresport-Api.zip 
}

deployService() {
    aws deploy create-deployment --application-name ParesportApi --deployment-config-name CodeDeployDefault.OneAtATime --deployment-group-name ParesportApi --s3-location bucket=paresport-api,bundleType=zip,key=Paresport-Api.zip
}

help() {
    echo "Usage: $0 [option] : Start the paresport API container"
    echo
    echo "      --help          Display help for this command"
    echo "      --deploy        Deploy service on aws EC2"
    echo "      --push          Push service on aws bucket"
}

while [ $# -ne 0 ]; do
    case "$1" in
        --help)
            help
            exit 0
            shift
            ;;
        --deploy)
            deployService
            exit 0
            shift
            ;;
        --push)
            pushService
            exit 0
            shift
            ;;
        --build)
            buildContainer
            exit 0
            shift
            ;;
        --start)
            startContainer
            exit 0
            shift
            ;;
        --)
            shift
            break
            ;;
        -*)
            echo "Error : Unknown option: $1" >&2
            exit 1
            ;;
        *)
            break
            ;;
    esac
done
