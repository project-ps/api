const mongoose = require('mongoose');
const Matches = require('../schemas/matches.schema');
const PandascoreApi = require('./pandascore.service');
const config = require("../../app-config");

module.exports = class MatchesService {
    pandascoreApi = new PandascoreApi();
    mongoDB = config.mongo.uri;
    options = {
        user: config.mongo.user,
        pass: config.mongo.pass,
        server: config.mongo.server,
        replset: config.mongo.replset
    };
    matches = [];

    constructor() { }

    connect() {
        mongoose.connect(this.mongoDB, this.options);
    }

    async callApi(options) {
        const query = `&page=${options.page}&range[begin_at]=${options.start},${options.end}`
        await this.pandascoreApi.getMatches(options.path, query).then((resp) => {
            if (resp.length === 0) {
                return;
            } else {
                for (let r of resp) {
                    this.matches.push(r);
                }
                options.page = options.page + 1;
                return this.callApi(options);
            }
        });
    }

    async push(options) {
        this.matches = []
        this.connect();
        const that = this;
        mongoose.connection.on('connected', async function (error) {
            await that.callApi(options);            
            if (that.matches.length > 0) {
                for (let match of that.matches) {             
                    Matches.findOne({ 'id': match.id }, (error , matchExist) => { 
                        if(matchExist) {
                            if (matchExist.modified_at != match.modified_at) {
                                matchExist.update(match, (error) => {
                                    if (error) {
                                        console.log(error);
                                    }
                                });
                            }
                        } else {
                            const m = new Matches(match)
                            m.save((error) => {
                                if (error) {
                                    console.log(error);
                                }
                            });
                        }
                    });
                }
            }
        });
        mongoose.connection.on('error', function (error) {
            console.log(error);
        });
    }
}