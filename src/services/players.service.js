const mongoose = require('mongoose');
const PandascoreApi = require('./pandascore.service');
const config = require("../../app-config");
const Players = require('../schemas/players.schema');



module.exports = class PlayersService {
    pandascoreApi = new PandascoreApi();
    mongoDB = config.mongo.uri;
    options = {
        user: config.mongo.user,
        pass: config.mongo.pass,
        server: config.mongo.server,
        replset: config.mongo.replset
    };
    players = [];

    constructor() { }

    connect() {
        mongoose.connect(this.mongoDB, this.options);
    }

    
    async callApi(options) {
        const query = `&page=${options.page}`
        await this.pandascoreApi.getData(options.path, query).then((resp) => {
            if (resp.length === 0) {
                return;
            } else {  
                for (let r of resp) {
                    this.players.push(r);
                }
                options.page = options.page + 1;
                return this.callApi(options);
            }
        });
    }

    async push(options) {        
        this.players = []
        this.connect();
        const that = this;
        mongoose.connection.on('connected', async function (error) {
            await that.callApi(options);
            if (that.players.length > 0) {
                for (let player of that.players) {   
                    Players.findOne({ 'id': player.id }, (error , playerExist) => {
                        if(!playerExist) {
                            const p = new Players(player);
                            p.save((error) => {
                                if (error) {
                                    console.log(error);
                                }
                            });
                        } 
                    });
                }
            }
        });
        mongoose.connection.on('erroror', function (error) {
            console.log(error);
        });
    }
}