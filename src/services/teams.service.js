const mongoose = require('mongoose');
const Teams = require('../schemas/teams.schema');
const PandascoreApi = require('./pandascore.service');
const config = require("../../app-config");

module.exports = class TeamsService {
    pandascoreApi = new PandascoreApi();
    mongoDB = config.mongo.uri;
    options = {
        user: config.mongo.user,
        pass: config.mongo.pass,
        server: config.mongo.server,
        replset: config.mongo.replset
    };
    teams = [];

    constructor() { }

    connect() {
        mongoose.connect(this.mongoDB, this.options);
    }

    async callApi(options) {
        const query = `&page=${options.page}`
        await this.pandascoreApi.getData(options.path, query).then((resp) => {
            console.log(resp);
            if (resp.length === 0) {
                return;
            } else {                
                for (let r of resp) {
                    this.teams.push(r);
                }
                options.page = options.page + 1;
                return this.callApi(options);
            }
        });
    }

    async push(options) {
        this.teams = []
        this.connect();
        const that = this;
        mongoose.connection.on('connected', async function (error) {
            await that.callApi(options);
            if (that.teams.length > 0) {
                for (let team of that.teams) {    
                    Teams.findOne({ 'id': team.id }, (error , teamExist) => {
                        if(teamExist) {
                            if (teamExist.modified_at != team.modified_at) {
                                teamExist.update(team, (error) => {
                                    if (error) {
                                        console.log(error);
                                    }
                                });
                            }
                        } else {
                            const t = new Teams(team);
                            t.save((error) => {
                                if (error) {
                                    console.log(error);
                                }
                            });
                        }
                    });
                }
            }
        });
        mongoose.connection.on('error', function (error) {
            console.log(error);
        });
    }
}