const fetch = require("node-fetch");
const config = require("../../app-config");

module.exports = class PandascoreApi {

    uri = config.api.uri;
    token = config.api.token;

    constructor() { }

    async getMatches(path, query = '') {    
        return fetch(this.uri + path + '?token=' + this.token + '&per_page=150&sort=begin_at' + query)
            .then(response => {
                return response.json().then((d) => {
                    return d;
                });
            })
    }

    async getData(path, query = '') {
        return fetch(this.uri + path + '?token=' + this.token + query)
        .then(response => {
            return response.json().then((d) => {
                return d;
            });
        })
    }
}



