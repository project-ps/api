const mongoose = require('mongoose');

const playerSchema = mongoose.Schema({
    birth_year: Number,
    birthday: String,
    id: Number,
    current_team: Object,
    current_videogame: Object,
    first_name: String,
    hometown: String,
    image_url: String,
    last_name: String,
    name: String,
    nationality: String,
    role: String,
    slug: String,
})

module.exports = mongoose.model('Players', playerSchema);