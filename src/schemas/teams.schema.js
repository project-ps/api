const mongoose = require('mongoose');

const teamsSchema = mongoose.Schema({
    acronym: String,
    current_videogame: Object,
    id: Number,
    image_url: String,
    location: String,
    modified_at: String,
    name: String,
    players: Array,
    slug: String
})

module.exports = mongoose.model('Teams', teamsSchema);