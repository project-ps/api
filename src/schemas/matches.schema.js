const mongoose = require('mongoose');

const matchesSchema = mongoose.Schema({
    begin_at: { type: String, required: true },
    detailed_stats: Boolean,
    draw: Boolean,
    end_at: String,
    forfeit: Boolean,
    game_advantage: String,
    games: Array,
    id: { type: Number, required: true },
    league: Object,
    league_id: Number,
    live: Object,
    live_url: String,
    match_type: String,
    modified_at: String,
    name: String,
    number_of_games: Number,
    opponents: Array,
    rescheduled: Boolean,
    results: Array,
    scheduled_at: String,
    serie: Object,
    serie_id: Number,
    slug: String,
    status: String,
    tournament: Object,
    tournament_id: Number,
    videogame: Object,
    videogame_version: Object,
    winner: Object,
    winner_id: Number
});

module.exports = mongoose.model('Matches', matchesSchema);