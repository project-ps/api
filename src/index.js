const express = require('express');
const schedule = require('node-schedule');
const config = require('../app-config');
const MatchesService = require('./services/matches.service');
const PlayersService = require('./services/players.service');
const TeamsService = require('./services/teams.service');

const teamsService = new TeamsService();
const playersService = new PlayersService();
const matchesService = new MatchesService();
const port = 3000;
const app = express();

schedule.scheduleJob('0 */2 * * *', () => {
    const matchesOptions = {
		page: 1,
		start: config.api.startDate.toISOString(),
		end: config.api.endDate.toISOString(),
		path: 'matches/'
	}
    matchesService.push(matchesOptions)
    
    const playersOptions = {
		page: 1,
		start: config.api.startDate.toISOString(),
		end: config.api.endDate.toISOString(),
		path: 'players/'
	}
    playersService.push(playersOptions)
    
    const teamsOptions = {
		page: 1,
		start: config.api.startDate.toISOString(),
		end: config.api.endDate.toISOString(),
		path:  'teams/'
	}
	teamsService.push(teamsOptions)
});

app.listen(port);