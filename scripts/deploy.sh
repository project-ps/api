#!/bin/bash

set -e

./scripts/disableHostKeyChecking.sh

eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

DEPLOY_SERVERS=$DEPLOY_SERVERS

ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

for server in "${ALL_SERVERS[@]}"
do
  echo "prepare server to ${server}"
  ssh ec2-user@${server} 'bash' < ./scripts/prepareServer.sh
done

for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  ssh ec2-user@${server} 'bash' < ./scripts/updateAndRestart.sh
done
