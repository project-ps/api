#!/bin/bash 

# Create security group 
aws ec2 create-security-group --group-name paresport-api --description "Security group for Paresport API"
aws ec2 authorize-security-group-ingress --group-name paresport-api --protocol tcp --port 22 --cidr 0.0.0.0/0
aws ec2 authorize-security-group-ingress --group-name paresport-api --protocol tcp --port 80 --cidr 0.0.0.0/0

# Create EC2 instance 
aws ec2 run-instances --image-id ami-007fae589fdf6e955 --count 1 --instance-type t2.micro --key-name ajccloud --security-groups paresport-api 
--tag-specifications 'ResourceType=instance,Tags=[{Key=webserver,Value=production}]'


echo aws ec2 describe-instances --filters "Name=tag:webserver,Values=production" --query 'Reservations[*].Instances[*].PublicIpAddress'
