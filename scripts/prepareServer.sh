#!/bin/bash

# any future command that fails will exit the script
set -e

sudo yum -y update &&  sudo yum install -y git docker 
sudo systemctl start docker 
sudo usermod -aG docker $USER