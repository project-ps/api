#!/bin/bash

set -e
rm -rf /home/ec2-user/api
git clone https://gitlab.com/project-ps/api.git
cd api 
sudo ./api.sh --build 
sudo ./api.sh --start